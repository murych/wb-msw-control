#include "lib.hpp"

auto main() -> int
{
  auto const lib = library {};

  return lib.name == "wb-msw-control" ? 0 : 1;
}
