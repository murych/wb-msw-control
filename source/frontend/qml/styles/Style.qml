pragma Singleton

import QtQuick 2.15

Item {
    property alias fontAwesome: fontAwesomeLoader.name
    readonly property color colorBackground: "#f4c842"

    FontLoader {
        id: fontAwesomeLoader
        source: "qrc:/fonts/fontawesome-regular.ttf"
    }
}
