import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import styles 1.0

ApplicationWindow {
  visible: true
  width: 640
  height: 480
  title: qsTr("Client Management")

  id: window

  Connections {
    target: masterController.ui_navigationController
    onGoDashboardView: function () {
      contentFrame.replace("qrc:/views/DashboardView.qml")
    }
    onGoDeviceView: function () {
      contentFrame.replace("qrc:/views/DeviceView.qml")
    }
    onGoSettingsView: function () {
      contentFrame.replace("qrc:/views/SettingsView.qml")
    }
  }

  StackView {
    id: contentFrame
    initialItem: "qrc:/views/SplashView.qml"
    anchors {
      top: header.bottom
      bottom: parent.bottom
      right: parent.right
      left: parent.left
    }
  }

  Component.onCompleted: function () {
    contentFrame.replace("qrc:/views/DashboardView.qml")
  }

  header: ToolBar {
    Material.background: Material.LightGreen
    ToolButton {
      id: menuButton
      anchors.left: parent.left
      anchors.verticalCenter: parent.verticalCenter
      text: "\uf0c9"
      font.pixelSize: 20
      font.family: Style.fontAwesome
      onClicked: drawer.open()
    }
    Label {
      id: sectionLabel
      anchors.centerIn: parent
      text: "Wirenboard demo"
      font.pixelSize: 20
      elide: Label.ElideRight
    }
  }

  Drawer {
    id: drawer
    width: Math.min(window.width, window.height) / 3 * 2
    height: window.height

    ListView {
      focus: true
      currentIndex: -1
      anchors.fill: parent

      delegate: ItemDelegate {
        width: parent.width
        height: model.height
        text: model.text
        icon.name: model.icon
        highlighted: ListView.isCurrentItem
        onClicked: function () {
          drawer.close()
          model.triggered()
        }
      }

      model: ListModel {
        ListElement {
          icon: "7zip"
          text: qsTr("Dashboard")
          triggered: function () {
            masterController.ui_navigationController.goDashboardView()
          }
        }
        ListElement {
          text: qsTr("Device")
          triggered: function () {
            masterController.ui_navigationController.goDeviceView()
          }
        }
        ListElement {
          text: qsTr("Settings")
          triggered: function () {
            masterController.ui_navigationController.goSettingsView()
          }
        }
      }

      ScrollIndicator.vertical: ScrollIndicator {}
    }
  }
}
