#include <QApplication>

#include "backend.h"
#include "controllers/master_controller.h"
#include "controllers/navigation_controller.h"
#include "device/msw.h"
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <memory>

auto
main(int argc, char* argv[]) -> int
{
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication app{ argc, argv };

  QIcon::setThemeName("Mint-Y-Dark");

  QQmlApplicationEngine engine;
  engine.addImportPath("qrc:/");
  QQuickStyle::setStyle("Material");

  qmlRegisterType<wb::device::MSW>("org.example", 1, 0, "DeviceModel");

  qmlRegisterType<wb::controllers::MasterController>(
    "WB", 1, 0, "MasterController");
  qmlRegisterType<wb::controllers::NavigationController>(
    "WB", 1, 0, "NavigationController");
  wb::controllers::MasterController master_controller;
  engine.rootContext()->setContextProperty("masterController",
                                           &master_controller);

  const QUrl url{ QStringLiteral("qrc:/main") };
  QObject::connect(
    &engine,
    &QQmlApplicationEngine::objectCreated,
    &app,
    [url](QObject* object, const QUrl& object_url) {
      if (object == nullptr && url == object_url) {
        QApplication::exit(-1);
      }
    },
    Qt::QueuedConnection);
  engine.load(url);

  wb::ApplicationBackend backend{ "/dev/ttyUSB0" };
  // backend.startPolling();
  return QApplication::exec();
}
