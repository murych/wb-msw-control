#ifndef BACKEND_TRANSFER_SERIALPARAMS_H
#define BACKEND_TRANSFER_SERIALPARAMS_H

#include <string>

namespace wb::transfer {

struct SerialParams
{
  std::string port;
  int baudRate;
  int parity;
  int dataBits;
  int stopBits;
};

} // namespace wb::transfer

#endif // BACKEND_TRANSFER_SERIALPARAMS_H
