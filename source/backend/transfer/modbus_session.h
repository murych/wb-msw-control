#ifndef BACKEND_TRANSFER_MODBUSSESSION_H
#define BACKEND_TRANSFER_MODBUSSESSION_H

#include "protocol/modbus/modbus_defs.h"
#include "serial_params.h"
#include "session_interface.h"
#include <QQueue>
#include <QTimer>
#include <gsl-lite/gsl-lite.hpp>
#include <memory>
#include <modbus.h>

namespace wb {

namespace device {
class MSW;
} // namespace device

namespace command::modbus {
class OperationModbus;
} // namespace command::modbus

namespace command::wirenboard {
class GetCurrentTemperature;
class GetFirmwareBuildInfo;
class GetFirmwareVersion;
} // namespace command::wirenboard

namespace transfer {
class MswSession : public SessionInterface
{
  Q_OBJECT
public:
  auto getCurrentTemperature(gsl::owner<device::MSW*> device)
    -> command::wirenboard::GetCurrentTemperature*;

  auto getFirmwareBuildInfo() -> command::wirenboard::GetFirmwareBuildInfo*;
  auto getFirmwareVersion() -> command::wirenboard::GetFirmwareVersion*;

  enum class ErrorType
  {
    NoError,
    UnableToCreateModbusHandle,
    InvalidSlaveId,
    ConnectionFailed
  };
  using Error = std::pair<ErrorType, QString>;

  MswSession(const MswSession&)                = delete;
  MswSession(MswSession&&) noexcept            = delete;
  MswSession& operator=(const MswSession&)     = delete;
  MswSession& operator=(MswSession&&) noexcept = delete;

  explicit MswSession(SerialParams&& params, QObject* parent = nullptr);
  ~MswSession() override;

  void setSerialParams(SerialParams&& params);

  auto start() -> Error;
  void stop();

  void setSlaveId(int new_slave_id) { m_slaveId = new_slave_id; }
  [[nodiscard]] auto slaveId() const { return m_slaveId; }

  [[nodiscard]] auto packets() const { return m_packets; }
  [[nodiscard]] auto errors() const { return m_errors; }

  [[nodiscard]] auto isSessionUp() const
  {
    return m_sessionState == SessionState::Idle ||
           m_sessionState == SessionState::Running;
  }

private:
  protocol::modbus::ModbusCtx m_modbus{ nullptr };

  int m_slaveId{ 0 };

  int m_packets{ 0 };
  int m_errors{ 0 };
  int m_timeOut{ 0 };
  bool m_isTransactionPending{ false };

  SessionState m_sessionState{ SessionState::Stopped };
  void setSessionState(SessionState new_state) { m_sessionState = new_state; }

  QQueue<command::modbus::OperationModbus*> m_queue;

  static const int libmodbus_error_code{ -1 };

  template<class T, typename... Args>
  T* enqueueOperation(Args&&... args)
  {
    auto* operation{ new T{ args... } };
    m_queue.enqueue(operation);

    if (m_sessionState == SessionState::Idle) {
      QTimer::singleShot(0, this, &MswSession::processQueue);
      setSessionState(SessionState::Running);
    }

    return operation;
  }

  command::modbus::OperationModbus* m_currentOperation{ nullptr };

signals:
  void sessionStateChanged(SessionState state);

private slots:
  void processQueue() override;
  void serialTransfer() override;

  void onCurrentOperationFinished() override;
};

} // namespace transfer

} // namespace wb

#endif // BACKEND_TRANSFER_MODBUSSESSION_H
