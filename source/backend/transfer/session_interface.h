#ifndef BACKEND_TRANSFER_SESSIONINTERFACE_H
#define BACKEND_TRANSFER_SESSIONINTERFACE_H

#include <QObject>

namespace wb::transfer {

class SessionInterface : public QObject
{
  Q_OBJECT
public:
  enum class SessionState
  {
    Stopped,
    Idle,
    Running
  };

  ~SessionInterface() override                             = default;
  SessionInterface(const SessionInterface&)                = delete;
  SessionInterface(SessionInterface&&) noexcept            = delete;
  SessionInterface& operator=(const SessionInterface&)     = delete;
  SessionInterface& operator=(SessionInterface&&) noexcept = delete;

protected:
  using QObject::QObject;

signals:
  void connected();
  void disconnected();

protected slots:
  virtual void processQueue()               = 0;
  virtual void serialTransfer()             = 0;
  virtual void onCurrentOperationFinished() = 0;
};

} // namespace wb::transfer

#endif // BACKEND_TRANSFER_SESSIONINTERFACE_H
