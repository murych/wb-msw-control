#include "modbus_session.h"
#include "command/modbus/operation_modbus.h"
#include "command/wirenboard/get_current_temperature_operation.h"
#include "command/wirenboard/get_firmware_build_info_operation.h"
#include "command/wirenboard/get_firmware_version_operation.h"
#include "transfer/serial_params.h"
#include "transfer/session_interface.h"
#include <QDebug>
#include <modbus.h>

namespace wb::transfer {

MswSession::MswSession(SerialParams&& params, QObject* parent)
  : SessionInterface{ parent }
  , m_modbus{ modbus_new_rtu(params.port.data(),
                             params.baudRate,
                             'N',
                             params.dataBits,
                             params.stopBits) }
{
}

MswSession::~MswSession()
{
  stop();
  m_modbus = nullptr;
}

MswSession::Error
MswSession::start()
{
  qDebug() << this << "START";

  if (m_modbus == nullptr) {
    return std::make_pair(ErrorType::UnableToCreateModbusHandle,
                          QStringLiteral("Unable to create modbus handle"));
  }

  if (modbus_set_slave(m_modbus, m_slaveId) == libmodbus_error_code) {
    modbus_free(m_modbus);
    return std::make_pair(ErrorType::InvalidSlaveId,
                          QStringLiteral("Invalid slave ID"));
  }

  if (modbus_connect(m_modbus) == libmodbus_error_code) {
    modbus_free(m_modbus);
    return std::make_pair(ErrorType::ConnectionFailed,
                          QStringLiteral("Connection failed"));
  }

  m_sessionState = SessionState::Idle;
  return std::make_pair(ErrorType::NoError, QLatin1String());
}

void
MswSession::stop()
{
  if (m_modbus != nullptr && m_sessionState != SessionState::Stopped) {
    qDebug() << this << "STOP";
    modbus_close(m_modbus);
    modbus_free(m_modbus);
  }

  m_sessionState = SessionState::Stopped;
}

void
MswSession::processQueue()
{
  if (m_queue.isEmpty()) {
    setSessionState(SessionState::Idle);
    return;
  }

  m_currentOperation = m_queue.dequeue();

  connect(m_currentOperation,
          &command::OperationInterface::finished,
          this,
          &MswSession::onCurrentOperationFinished);
  m_currentOperation->start();

  serialTransfer();
}

void
MswSession::serialTransfer()
{
  if (m_currentOperation == nullptr) {
    return;
  }

  qDebug() << this << "SERIAL TRANSFER" << m_currentOperation->description();
  auto result{ m_currentOperation->performOperation(m_modbus) };
  if (!result.has_value()) {
    qWarning() << this << "NO RESULT";
    return;
  }

  m_currentOperation->feedResponse(std::move(result));
}

void
MswSession::onCurrentOperationFinished()
{
  m_currentOperation->deleteLater();
  m_currentOperation = nullptr;

  QTimer::singleShot(0, this, &MswSession::processQueue);
}

auto
MswSession::getCurrentTemperature(gsl::owner<device::MSW*> device)
  -> command::wirenboard::GetCurrentTemperature*
{
  return enqueueOperation<command::wirenboard::GetCurrentTemperature>(device);
}

auto
MswSession::getFirmwareBuildInfo() -> command::wirenboard::GetFirmwareBuildInfo*
{
  return enqueueOperation<command::wirenboard::GetFirmwareBuildInfo>();
}

auto
MswSession::getFirmwareVersion() -> command::wirenboard::GetFirmwareVersion*
{
  return enqueueOperation<command::wirenboard::GetFirmwareVersion>();
}

}
