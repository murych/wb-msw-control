#ifndef BACKEND_DEVICE_MSW_H
#define BACKEND_DEVICE_MSW_H

#include "wb-msw-lib_export.h"
#include <QObject>
#include <memory>

namespace wb::device {

class WB_MSW_LIB_EXPORT MSW : public QObject
{
  Q_OBJECT
  Q_PROPERTY(double ui_currentTemperature READ currentTemperature WRITE
               setCurrentTemperature NOTIFY currentTemperatureChanged)
public:
  MSW(QObject* parent = nullptr);
  ~MSW() override;

  MSW(const MSW&)            = delete;
  MSW(MSW&&)                 = delete;
  MSW& operator=(const MSW&) = delete;
  MSW& operator=(MSW&&)      = delete;

  [[nodiscard]] auto currentTemperature() const { return m_currentTemperature; }

private:
  double m_currentTemperature{ 0.0 };

signals:
  void currentTemperatureChanged();

public slots:
  void setCurrentTemperature(double new_temperature);
};

} // namespace wb::device

#endif // BACKEND_DEVICE_MSW_H
