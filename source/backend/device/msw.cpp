#include "msw.h"

#include <QDebug>

namespace wb::device {

MSW::MSW(QObject* parent)
  : QObject{ parent }
{
}

MSW::~MSW() = default;

void
MSW::setCurrentTemperature(double new_temperature)
{
  qDebug() << this << "SET CURRENT TEMPERATURE" << new_temperature;
  if (!qFuzzyCompare(m_currentTemperature, new_temperature)) {
    m_currentTemperature = new_temperature;
    emit currentTemperatureChanged();
  }
}


} // namespace wb::device
