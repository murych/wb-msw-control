#include "backend.h"

#include "device/msw.h"
#include "transfer/modbus_session.h"
#include "transfer/serial_params.h"
#include <QDebug>
#include <QTimer>
#include <chrono>

namespace {
const std::chrono::milliseconds INTERVAL{ 500 };
}

namespace wb {

ApplicationBackend::ApplicationBackend(const QString& port, QObject* parent)
  : QObject{ parent }
  , m_device{ std::make_unique<device::MSW>(this) }
  , m_session{ std::make_unique<transfer::MswSession>(
      transfer::SerialParams{ port.toStdString(), 9600, 0, 8, 2 },
      this) }
  , m_pollTimer{ std::make_unique<QTimer>() }
{
  qDebug() << this << "INIT" << m_device.get() << m_session.get();

  m_session->setSlaveId(15);

  m_pollTimer->setInterval(INTERVAL);
  m_pollTimer->setSingleShot(false);

  connect(m_pollTimer.get(),
          &QTimer::timeout,
          this,
          &ApplicationBackend::pollRoutine);

  auto started{ m_session->start() };
  if (started.first != transfer::MswSession::ErrorType::NoError) {
    qWarning() << this << "SESSION INIT ERROR:" << started.second;
    m_session->stop();
  }
}

ApplicationBackend::~ApplicationBackend() = default;

void
ApplicationBackend::startPolling()
{
  qDebug() << this << "start polling";
  m_pollTimer->start();
}

void
ApplicationBackend::stopPolling()
{
  qDebug() << this << "stop polling";
  m_pollTimer->stop();
}

void
ApplicationBackend::pollRoutine()
{
  qDebug() << this << "poll routine";
  m_session->getCurrentTemperature(m_device.get());

  m_session->getFirmwareVersion();
  m_session->getFirmwareBuildInfo();
}

}
