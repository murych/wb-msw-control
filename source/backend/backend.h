#ifndef BACKEND_BACKEND_H
#define BACKEND_BACKEND_H

#include "wb-msw-lib_export.h"
#include <QObject>
#include <gsl-lite/gsl-lite.hpp>
#include <memory>

class QTimer;

namespace wb {

namespace transfer {
class MswSession;
} // namespace transfer

namespace device {
class MSW;
} // namespace device

class WB_MSW_LIB_EXPORT ApplicationBackend : public QObject
{
  Q_OBJECT

public:
  ApplicationBackend(const QString& port, QObject* parent = nullptr);
  ~ApplicationBackend() override;

  [[nodiscard]] auto device() const
  {
    return gsl::owner<device::MSW*>{ m_device.get() };
  }

private:
  std::unique_ptr<device::MSW> m_device{ nullptr };
  std::unique_ptr<transfer::MswSession> m_session{ nullptr };
  std::unique_ptr<QTimer> m_pollTimer{ nullptr };

public slots:
  void startPolling();
  void stopPolling();

private slots:
  void pollRoutine();
};

} // namespace wb

#endif // BACKEND_BACKEND_H
