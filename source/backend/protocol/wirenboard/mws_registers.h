#ifndef BACKEND_PROTOCOL_WIRENBOARD_MWSREGISTERS_H
#define BACKEND_PROTOCOL_WIRENBOARD_MWSREGISTERS_H

#include <cstdint>

namespace wb::protocol::wirenboard {

enum class MwsCoils : uint16_t
{
  //! @brief Включение пищалки (buzzer)
  //! @details
  BuzzerEnable                  = 0x0000,
  ForceCO2SensorRecalibration   = 0x0001,
  TurnHeaterInTemperatureSensoe = 0x0002,
  TurnC02Sensor                 = 0x0003,
  TurnRedLed                    = 0x000A,
  TurnGreenLed                  = 0x000B
};

enum class MwsInputRegisters : uint16_t
{
  //! @brief Температура
  //! @details x0.1, °C. Error: 0x7FFF
  Temperature               = 0x0000,
  RelativeHumidity          = 0x0001,
  NoiseLevel                = 0x0003,
  TemperatureThousands      = 0x0004,
  RelativeHumidityThousands = 0x0005,
  CO2Concentration          = 0x0008,
  LuminosityMSB             = 0x0009,
  LuminosityLSB             = 0x000A
};

enum class MwsHoldingRegisters : uint16_t
{
  ForceCO2Recalibration = 0x0058
};

}

#endif // BACKEND_PROTOCOL_WIRENBOARD_MWSREGISTERS_H
