#ifndef BACKEND_PROTOCOL_WIRENBOARD_WIRENBOARDREGISTERS_H
#define BACKEND_PROTOCOL_WIRENBOARD_WIRENBOARDREGISTERS_H

#include <cstdint>

namespace wb::protocol::wirenboard {

enum class WirenboardInputRegisters : uint16_t
{
  TimeFromBootMSB = 0x0068,
  TimeFromBootLSB = 0x0069,
};

enum class WirenBoardHoldingRegisters : uint16_t
{
  PowerSupplyVoltage                  = 0x0079,
  CommunicationPortSpeed              = 0x006E,
  CommunicationPortParity             = 0x006F,
  CommunicationPortStopBits           = 0x0070,
  CommunicationPortTimeBeforeResponse = 0x0071,
  ContinousRegistersReadMode          = 0x0072,
  CommunicationAddress                = 0x0080,
  RebootDevice                        = 0x0078,
  FirmwareUpdateMode                  = 0x0081,
  MicrocontrollerVoltage              = 0x007B,
  MicrocontrollerTemperature          = 0x007C,
  DeviceModel                         = 0x00C8,
  DeviceModelExtension                = 0x00CE,
  FirmwareGitCommitHashAndBranchName  = 0x00DC,
  FirmwareVersion                     = 0x00FA,
  SerialNumberExtension               = 0x010A,
  SerialNumber                        = 0x010E,
  FirmwareSignature                   = 0x0122,
  BootloaderVersion                   = 0x014A
};

enum class WirenBoardVersionInputRegisters : uint16_t
{
  DeviceModel              = 0x00C8,
  FirmwareVersionMajor     = 0x0140,
  FirmwareVersionMinor     = 0x0141,
  FirmwareVersionPatch     = 0x0142,
  FirmwareVersionSuffix    = 0x0143,
  FirmwareVersion          = 0x0144,
  FirmwareVersionBigEndian = 0x0146
};

}

#endif // BACKEND_PROTOCOL_WIRENBOARD_WIRENBOARDREGISTERS_H
