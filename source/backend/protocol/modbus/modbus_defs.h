#ifndef BACKEND_PROTOCOL_MODBUS_MODBUSDEFS_H
#define BACKEND_PROTOCOL_MODBUS_MODBUSDEFS_H

#include <cstdint>
#include <gsl-lite/gsl-lite.hpp>
#include <modbus.h>
#include <optional>
#include <vector>

namespace wb::protocol::modbus {

using ModbusPayload = std::optional<std::vector<uint16_t>>;
using ModbusCtx     = gsl::owner<modbus_t*>;

}

#endif // BACKEND_PROTOCOL_MODBUS_MODBUSDEFS_H
