#ifndef BACKEND_CONTROLLERS_MASTERCONTROLLER_H
#define BACKEND_CONTROLLERS_MASTERCONTROLLER_H

#include "wb-msw-lib_export.h"
#include <QObject>
#include <memory>

namespace wb::controllers {

class NavigationController;

class WB_MSW_LIB_EXPORT MasterController : public QObject
{
  Q_OBJECT
  Q_PROPERTY(wb::controllers::NavigationController* ui_navigationController READ
               navigationController CONSTANT)
public:
  explicit MasterController(QObject* parent = nullptr);
  ~MasterController() override;

  MasterController(const MasterController&)                        = delete;
  MasterController(MasterController&&) noexcept                    = delete;
  auto operator=(const MasterController&) -> MasterController&     = delete;
  auto operator=(MasterController&&) noexcept -> MasterController& = delete;

  [[nodiscard]] NavigationController* navigationController() const;

private:
  struct Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace wb::controllers

#endif // BACKEND_CONTROLLERS_MASTERCONTROLLER_H
