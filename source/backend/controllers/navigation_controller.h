#ifndef BACKEND_CONTROLLERS_NAVIGATIONCONTROLLER_H
#define BACKEND_CONTROLLERS_NAVIGATIONCONTROLLER_H

#include "wb-msw-lib_export.h"
#include <QObject>

namespace wb::controllers {

class WB_MSW_LIB_EXPORT NavigationController : public QObject
{
  Q_OBJECT
public:
  using QObject::QObject;

signals:
  void goDashboardView();
  void goDeviceView();
  void goSettingsView();
};

}

#endif // BACKEND_CONTROLLERS_NAVIGATIONCONTROLLER_H
