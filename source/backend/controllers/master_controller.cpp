#include "master_controller.h"

#include "navigation_controller.h"

namespace wb::controllers {

struct MasterController::Implementation
{
  Implementation(MasterController* master_controller)
    : m_navigationController{ std::make_unique<NavigationController>(
        master_controller) }
    , m_masterController{ master_controller }
  {
  }
  ~Implementation() = default;

  std::unique_ptr<NavigationController> m_navigationController{ nullptr };

private:
  MasterController* m_masterController{ nullptr };
};

MasterController::MasterController(QObject* parent)
  : QObject{ parent }
  , m_implementation{ std::make_unique<Implementation>(this) }
{
}

MasterController::~MasterController() = default;

NavigationController*
MasterController::navigationController() const
{
  return m_implementation->m_navigationController.get();
}

}
