#ifndef BACKEND_COMMAND_OPERATIONINTERFACE_H
#define BACKEND_COMMAND_OPERATIONINTERFACE_H

#include "wb-msw-lib_export.h"
#include <QObject>

namespace wb::command {

class WB_MSW_LIB_NO_EXPORT OperationInterface : public QObject
{
  Q_OBJECT
public:
  enum BasicOperationState
  {
    Ready = 0,
    Finished,
    User
  };

  using QObject::QObject;
  ~OperationInterface() override = default;

  [[nodiscard]] virtual auto description() const -> QString = 0;
  [[nodiscard]] virtual auto operationState() const -> int  = 0;

  virtual void setOperationState(int new_operation_state) = 0;

  virtual void start()  = 0;
  virtual void finish() = 0;

signals:
  void started();
  void finished();

protected slots:
  virtual void onOperationTimeout() = 0;
};

} // namespace wb::command

#endif // BACKEND_COMMAND_OPERATIONINTERFACE_H
