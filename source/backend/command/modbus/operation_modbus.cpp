#include "operation_modbus.h"

#include "command/operation_interface.h"
#include <QDebug>
#include <QTimer>

namespace wb::command::modbus {

OperationModbus::OperationModbus(QObject* parent)
  : OperationInterface{ parent }
  , m_timeoutTimer{ std::make_unique<QTimer>() }
{
  m_timeoutTimer->setInterval(m_interval);
  m_timeoutTimer->setSingleShot(true);

  connect(m_timeoutTimer.get(),
          &QTimer::timeout,
          this,
          &OperationModbus::onOperationTimeout);
}

OperationModbus::~OperationModbus() = default;

void
OperationModbus::start()
{
  if (!begin()) {
    finishLater();
    return;
  }

  setOperationState(Started);
  startTimeout();
}

void
OperationModbus::finish()
{
  setOperationState(OperationInterface::Finished);
  stopTimeout();
  emit finished();
}

void
OperationModbus::finishLater()
{
  QTimer::singleShot(0, this, &OperationInterface::finish);
}

void
OperationModbus::startTimeout()
{
  m_timeoutTimer->start();
}

void
OperationModbus::stopTimeout()
{
  m_timeoutTimer->stop();
}

void
OperationModbus::setTimeout(std::chrono::milliseconds timeout)
{
  m_timeoutTimer->setInterval(timeout);
}

void
OperationModbus::onOperationTimeout()
{
  qWarning() << this << "operation timeout";
  finish();
}

void
OperationModbus::feedResponse(protocol::modbus::ModbusPayload&& payload)
{
  if (!processResponse(std::move(payload))) {
    qWarning() << this << "ERROR WHILE PROCESSING RESPONSE";
  }

  finish();
}

}
