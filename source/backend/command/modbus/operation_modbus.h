#ifndef BACKEND_COMMAND_OPERATIONMODBUS_H
#define BACKEND_COMMAND_OPERATIONMODBUS_H

#include "command/operation_interface.h"
#include "protocol/modbus/modbus_defs.h"
#include "wb-msw-lib_export.h"
#include <chrono>
#include <cstdint>
#include <memory>
#include <modbus.h>
#include <optional>
#include <vector>

class QTimer;

namespace wb::command::modbus {

class WB_MSW_LIB_NO_EXPORT OperationModbus : public OperationInterface
{
  Q_OBJECT

  enum ModbusOperationState
  {
    Started = OperationInterface::User
  };

public:
  explicit OperationModbus(QObject* parent = nullptr);
  ~OperationModbus() override;

  OperationModbus(const OperationModbus&)                = delete;
  OperationModbus(OperationModbus&&) noexcept            = delete;
  OperationModbus& operator=(const OperationModbus&)     = delete;
  OperationModbus& operator=(OperationModbus&&) noexcept = delete;

  [[nodiscard]] int operationState() const override { return m_operationState; }
  void setOperationState(int state) override { m_operationState = state; }

  void start() override;
  void finish() override;

  virtual bool begin() { return true; }
  void finishLater();

  [[nodiscard]] bool isFinished() const
  {
    return operationState() == OperationInterface::Finished;
  }

  void startTimeout();
  void stopTimeout();
  void setTimeout(std::chrono::milliseconds timeout);

  [[nodiscard]] virtual auto performOperation(protocol::modbus::ModbusCtx ctx)
    -> protocol::modbus::ModbusPayload = 0;

  void feedResponse(protocol::modbus::ModbusPayload&& payload);

protected:
  [[nodiscard]] virtual auto processResponse(
    protocol::modbus::ModbusPayload&& payload) -> bool    = 0;
  [[nodiscard]] virtual auto startAddress() const -> int  = 0;
  [[nodiscard]] virtual auto numberOfBytes() const -> int = 0;

private:
  int m_operationState{ BasicOperationState::Ready };

  std::unique_ptr<QTimer> m_timeoutTimer{ nullptr };
  const std::chrono::milliseconds m_interval{ 100 };

protected slots:
  void onOperationTimeout() override;
};

}

#endif // BACKEND_COMMAND_OPERATIONMODBUS_H
