#include "get_current_temperature_operation.h"

#include "device/msw.h"
#include "protocol/wirenboard/mws_registers.h"
#include <QDebug>
#include <modbus.h>

namespace wb::command::wirenboard {

GetCurrentTemperature::GetCurrentTemperature(gsl::owner<device::MSW*> device,
                                             QObject* parent)
  : modbus::OperationModbus{ parent }
  , m_device{ device }
{
}

GetCurrentTemperature::~GetCurrentTemperature() = default;

protocol::modbus::ModbusPayload
GetCurrentTemperature::performOperation(protocol::modbus::ModbusCtx ctx)
{
  std::vector<uint16_t> payload;
  payload.resize(1);

  auto transaction_state{ modbus_read_input_registers(
    ctx, startAddress(), numberOfBytes(), payload.data()) };

  if (transaction_state == -1) {
    // set error?
    qWarning() << this << "perform operation"
               << "error in transaction?" << transaction_state;
    return std::nullopt;
  }

  qDebug() << this << payload.size();

  return payload;
}

int
GetCurrentTemperature::numberOfBytes() const
{
  return 1;
}

int
GetCurrentTemperature::startAddress() const
{
  return static_cast<int>(
    protocol::wirenboard::MwsInputRegisters::TemperatureThousands);
}

bool
GetCurrentTemperature::processResponse(
  protocol::modbus::ModbusPayload&& payload)
{
  if (!payload.has_value() || payload->empty()) {
    return false;
  }

  m_device->setCurrentTemperature(static_cast<double>(payload->at(0)) / 100.0);
  return true;
}

QString
GetCurrentTemperature::description() const
{
  return QStringLiteral("Requesting current temperature");
}

} // namespace wb::command::wirenboard
