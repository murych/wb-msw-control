#include "get_firmware_version_operation.h"

#include "protocol/wirenboard/wirenboard_registers.h"
#include <QDebug>

namespace wb::command::wirenboard {

// GetFirmwareVersion::GetFirmwareVersion(QObject* parent)
//   : modbus::OperationModbus{ parent }
// {
// }

GetFirmwareVersion::~GetFirmwareVersion() = default;

QString
GetFirmwareVersion::description() const
{
  return QStringLiteral("Request firmware version");
}

int
GetFirmwareVersion::startAddress() const
{
  return static_cast<int>(
    protocol::wirenboard::WirenboardInputRegisters::FirmwareVersion);
}

int
GetFirmwareVersion::numberOfBytes() const
{
  return 0x0109 - 0x00fa;
}

protocol::modbus::ModbusPayload
GetFirmwareVersion::performOperation(protocol::modbus::ModbusCtx ctx)
{
  std::vector<uint16_t> result;
  result.resize(numberOfBytes());

  auto transaction_state{ modbus_read_input_registers(
    ctx, startAddress(), numberOfBytes(), result.data()) };
  if (transaction_state == -1) {
    qWarning() << this << "perform operation"
               << "error in transaction?" << transaction_state;
    return std::nullopt;
  }

  qDebug() << this << result.size();
  return result;
}

bool
GetFirmwareVersion::processResponse(protocol::modbus::ModbusPayload&& payload)
{
  if (!payload.has_value()) {
    return false;
  }

  QByteArray text{ reinterpret_cast<const char*>(payload->data()),
                   static_cast<int>(payload->size()) };
  qDebug() << QString{ text }.replace(0x00, '.') << text.toHex(':');
  return true;
}

}
