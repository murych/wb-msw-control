#ifndef BACKEND_COMMAND_WIRENBOARD_MSW_GETFIRMWAREVERSIONOPERATION_H
#define BACKEND_COMMAND_WIRENBOARD_MSW_GETFIRMWAREVERSIONOPERATION_H

#include "command/modbus/operation_modbus.h"

namespace wb::command::wirenboard {

class GetFirmwareVersion : public command::modbus::OperationModbus
{
  Q_OBJECT
public:
  // explicit GetFirmwareVersion(QObject* parent = nullptr);
  using modbus::OperationModbus::OperationModbus;
  ~GetFirmwareVersion() override;

  [[nodiscard]] auto performOperation(protocol::modbus::ModbusCtx ctx)
    -> protocol::modbus::ModbusPayload override;

  [[nodiscard]] auto processResponse(protocol::modbus::ModbusPayload&& payload)
    -> bool override;

  [[nodiscard]] auto description() const -> QString override;

protected:
  [[nodiscard]] auto startAddress() const -> int override;
  [[nodiscard]] auto numberOfBytes() const -> int override;
};

} // namespace wb::command::wirenboard

#endif // BACKEND_COMMAND_WIRENBOARD_MSW_GETFIRMWAREBUILDINFOOPERATION_H
