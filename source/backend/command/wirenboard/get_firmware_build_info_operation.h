#ifndef BACKEND_COMMAND_WIRENBOARD_MSW_GETFIRMWAREBUILDINFOOPERATION_H
#define BACKEND_COMMAND_WIRENBOARD_MSW_GETFIRMWAREBUILDINFOOPERATION_H

#include "command/modbus/operation_modbus.h"

namespace wb::command::wirenboard {

class GetFirmwareBuildInfo : public command::modbus::OperationModbus
{
  Q_OBJECT
public:
  explicit GetFirmwareBuildInfo(QObject* parent = nullptr);
  ~GetFirmwareBuildInfo() override;

  protocol::modbus::ModbusPayload performOperation(
    protocol::modbus::ModbusCtx ctx) override;

  [[nodiscard]] auto processResponse(protocol::modbus::ModbusPayload&& payload)
    -> bool override;

  [[nodiscard]] auto description() const -> QString override;

protected:
  [[nodiscard]] auto startAddress() const -> int override;
  [[nodiscard]] auto numberOfBytes() const -> int override;
};

} // namespace wb::command::wirenboard

#endif // BACKEND_COMMAND_WIRENBOARD_MSW_GETFIRMWAREBUILDINFOOPERATION_H
