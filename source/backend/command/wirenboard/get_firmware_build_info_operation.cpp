#include "get_firmware_build_info_operation.h"

#include "protocol/modbus/modbus_defs.h"
#include "protocol/wirenboard/wirenboard_registers.h"
#include <QDebug>
#include <cstdint>
#include <modbus.h>
#include <optional>
#include <vector>

namespace wb::command::wirenboard {

GetFirmwareBuildInfo::GetFirmwareBuildInfo(QObject* parent)
  : command::modbus::OperationModbus{ parent }
{
}

GetFirmwareBuildInfo::~GetFirmwareBuildInfo() = default;

QString
GetFirmwareBuildInfo::description() const
{
  return QStringLiteral("Request firmware build info");
}

int
GetFirmwareBuildInfo::startAddress() const
{
  return static_cast<int>(protocol::wirenboard::WirenboardInputRegisters::
                            FirmwareGitCommitHashAndBranchName);
}

int
GetFirmwareBuildInfo::numberOfBytes() const
{
  return 0x00f8 - 0x00dc;
}

protocol::modbus::ModbusPayload
GetFirmwareBuildInfo::performOperation(protocol::modbus::ModbusCtx ctx)
{
  std::vector<uint16_t> result;
  result.resize(numberOfBytes());

  auto transaction_state{ modbus_read_input_registers(
    ctx, startAddress(), numberOfBytes(), result.data()) };
  if (transaction_state == -1) {
    qWarning() << this << "perform operation"
               << "error in transaction?" << transaction_state;
    return std::nullopt;
  }

  qDebug() << this << result.size();
  return result;
}

bool
GetFirmwareBuildInfo::processResponse(protocol::modbus::ModbusPayload&& payload)
{
  if (!payload.has_value()) {
    return false;
  }

  QByteArray text{ reinterpret_cast<const char*>(payload->data()),
                   static_cast<int>(payload->size()) };
  qDebug() << QString{ text } << text.toHex(':');
  return true;
}

} // namespace wb::command::wirenboard
