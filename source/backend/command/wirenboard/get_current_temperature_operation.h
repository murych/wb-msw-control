#ifndef BACKEND_COMMAND_WIRENBOARD_MSW_GETCURRENTTEMPERATUREOPERATION_H
#define BACKEND_COMMAND_WIRENBOARD_MSW_GETCURRENTTEMPERATUREOPERATION_H

#include "command/modbus/operation_modbus.h"
#include <algorithm>
#include <gsl-lite/gsl-lite.hpp>
#include <optional>

namespace wb::device {
class MSW;
} // namespace wb::device

namespace wb::command::wirenboard {

class GetCurrentTemperature : public modbus::OperationModbus
{
  Q_OBJECT
public:
  GetCurrentTemperature(const GetCurrentTemperature&)            = delete;
  GetCurrentTemperature(GetCurrentTemperature&&)                 = delete;
  GetCurrentTemperature& operator=(const GetCurrentTemperature&) = delete;
  GetCurrentTemperature& operator=(GetCurrentTemperature&&)      = delete;

  explicit GetCurrentTemperature(gsl::owner<device::MSW*> device,
                                 QObject* parent = nullptr);
  ~GetCurrentTemperature() override;

  protocol::modbus::ModbusPayload performOperation(
    protocol::modbus::ModbusCtx ctx) override;
  [[nodiscard]] bool processResponse(
    protocol::modbus::ModbusPayload&& payload) override;

  QString description() const override;

protected:
  [[nodiscard]] auto startAddress() const -> int override;
  [[nodiscard]] auto numberOfBytes() const -> int override;

private:
  gsl::owner<device::MSW*> m_device{ nullptr };
};

} // namespace wb::command::wirenboard

#endif // BACKEND_COMMAND_WIRENBOARD_MSW_GETCURRENTTEMPERATUREOPERATION_H
